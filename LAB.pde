class LAB {
  PFont typoAnneau;
  String mot = textFile[0];
  char [] lettres = mot.toCharArray();
  float segment;
  PVector position = new PVector(50,50);
  
//**************************************************************************************************************
//
//
//
//**************************************************************************************************************

  LAB() {
    String[] fontList = PFont.list();
    println(fontList);
    typoAnneau = createFont(fontList[numeroDePolice], taillePolice);
    textFont(typoAnneau, taillePolice);
    segment = 360f/lettres.length;

  }
  
//**************************************************************************************************************
//
//
//
//**************************************************************************************************************

  void display() {
    textSize(width/130);
    float arclength = 0;//----------------------- important d'initialiser à chaque passage
    for(int i=0; i<lettres.length; i++) {
      arclength += radians(segment);//----------- on ajoute la valeur de 360/nbre de lettre, en radians.
      float theta = PI - arclength;//------------ départ à PI (si on met + les lettres partent dans l'autre sens)
      pushMatrix();
      position.x = anneau.x + cos(theta)*(anneau.r/2);//----------- calcul la position x de chaque lettre
      position.y = anneau.y + sin(theta)*(anneau.r/2);//----------- calcul la position y de chaque lettre
      translate(position.x, position.y);
      
      rotate(theta + (3*PI)/2);//-------------------- la fameuse formule pour rotation des lettres
      fill(255);
      text(lettres[i], 0, 0);//------------------ on affiche les lettres sur le pourtour du cercle
      popMatrix();
    }
  }
}

