class MotClef {


  PFont typoClef;
  String clef, ref;
  float sizeLetter;
  float tailleMot;
  float hauteurMot;
  PVector position = new PVector();
  float distrib;

//**************************************************************************************************************
//
//
//
//**************************************************************************************************************

  MotClef(String _clef, String _ref, float _distrib) {
    clef = _clef;
    ref = _ref;//-------------------------------------------------------------------------------------------- on remet le nom du lab pour pouvoir gérer les hashmap
    distrib = _distrib;
    sizeLetter = taillePolice;
    String[] fontList = PFont.list();
    typoClef = createFont(fontList[numeroDePolice], taillePolice);
    textFont(typoClef, taillePolice);
    textSize(sizeLetter);
    tailleMot = textWidth(clef);
    float ascent = textAscent();
    float descent = textDescent();
    hauteurMot = (ascent + descent);
  }

  //******************************************************************************************************************************************
  //
  //
  //
  //******************************************************************************************************************************************

  void display(float a, int col, PVector pos) {//----------------------------------------------on recupere l'angle, la couleur, le coef pour le rayon et la position

    //for(int i=0; i<clef.length; i++) {
    float total =0, t =0;
    float x = 0;
    float angleDecalage = 0;
    float _tailleL[]  = new float[clef.length()];
    float _angleL [] = new float[clef.length()];//------------------------------------------------------------------------------------ angle pour chaque lettre
    char [] lettre = new char[clef.length()];
    x = textWidth(clef)/2;
    angleDecalage = x/((anneau.r/2)*distrib);//---------------------------------------------------------------------------------------- pourrecentrer le mot clefs avec le nom dulab

      for(int j=0; j<clef.length(); j++) {
      lettre [j] = clef.charAt(j);
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    for(int j=0;j<clef.length()/2 ; j++) {//----------------------------------------------------------------------------------------- petit algo pour permuter lordre deschar du tableau (merci developpez.net)
      char temporaire = lettre [j];
      lettre [j] = lettre [clef.length()-1 -j];
      lettre [clef.length()-1 -j] = temporaire;
    }  
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    for(int j=0; j<clef.length(); j++) {
      if(lettre[j] == 'i' || lettre[j] == 'I') {//--------------------------------------------------------------------------------si la lettre est un 'i', on rajoute a sa taille pour éviter qu'elle disparaisse
        _tailleL[j] = textWidth(lettre[j])+4;//+textWidth(part[i])/2;
      }
      else {
        _tailleL[j] = textWidth(lettre[j])*facteurScale;
      }
      total += _tailleL[j];
      _angleL[j] = total / ((anneau.r/2)*distrib);
      _angleL[j] -= angleDecalage ;

      position.x = anneau.x + cos(a + _angleL[j]) *((anneau.r/2)*distrib);//------------------------------------------------ calcul la position x de chaque mot-clef
      //position.x = anneau.x + cos(a + angleDecalage) *((anneau.r/2)*distrib);//------------------------------------------------ calcul la position x de chaque mot-clef
      //position.y = anneau.y + sin(a + angleDecalage) *((anneau.r/2)*distrib);//------------------------------------------------ calcul la position x de chaque mot-clef
      position.y = anneau.y + sin(a + _angleL[j]) *((anneau.r/2)*distrib);
      rectMode(CENTER);
      pushMatrix();
      translate(position.x, position.y);
      rotate(a+_angleL[j]+ (3*PI)/2);
      scale(policeScale);
      //      float transp = map(col,0,255,255,0);
      //      fill(60,134,0,transp);
      fill(col);
      // line(0,0,10,10);
      text(lettre[j], 0, 0);//+(i*hauteurMot)); 
      popMatrix();
      //          stroke(255,0,0,100);
      //      line(position.x, position.y, anneau.x, anneau.y);
    }
    //}
  }
}

