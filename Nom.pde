class Nom {
  PFont typoName;//--------------------------------------------------------------------------- typo du name
  char[] c;
  float[] angle;
  float[] x;
  float[] y;
  color couleur;
  float angleMoy = 0, xMoy = 0, yMoy = 0;//-------------------------------------------------- vriables utilisées pour placer les mots clef, définitons...
  float longueur;
  float [] tf;
  
//**************************************************************************************************************
//
//
//
//**************************************************************************************************************

  Nom(char[] _c, float[] _angle, float[] _x, float[] _y, color _col, float [] tailleFont) {
    c=_c;
    //c = reverse(c);
    angle=_angle;
    x=_x;
    y=_y;
    tf = tailleFont;
    couleur=_col;
    longueur = c.length;
    tf = tailleFont;
    //----------------------------------------------------------------------------------------- choix de la typo
    String[] fontList = PFont.list();
    for(int i=0 ; i<longueur ; i++) {
      typoName = createFont(fontList[numeroDePolice], tf[i]);
      textFont(typoName, tf[i]);
    }
  }
  
//**************************************************************************************************************
//
//
//
//**************************************************************************************************************

  void display() {

    for(int i=0 ; i<longueur ; i++) {
      textSize(tf[i]);
      pushMatrix();
      translate(x[i], y[i]);
      rotate(angle[i] + (3*PI)/2);
      //scale(policeScale);
      fill(couleur);
      text(c[i], 0, 0);
      popMatrix();
    }
  }
}

