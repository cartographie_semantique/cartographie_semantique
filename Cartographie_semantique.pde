import processing.pdf.*;

boolean savePDF = false;//----------------------------------------------------------------------------------- permet l'enregistrement format pdf

String [] nameLab = loadStrings("/home/algo/sketchbook/MCD LabToLab/cartoTextes/LabNames_bis");//------------ les differents fichiers txtutiliss
String [] textFile = loadStrings("/home/algo/sketchbook/MCD LabToLab/cartoTextes/LAB");
String [] adresses = loadStrings("/home/algo/sketchbook/MCD LabToLab/cartoTextes/Adresse");
String [] definitions = loadStrings("/home/algo/sketchbook/MCD LabToLab/cartoTextes/Definition");
String [] motCles = loadStrings("/home/algo/sketchbook/MCD LabToLab/cartoTextes/MotsClefs");
String [] encartTxt = loadStrings("/home/algo/sketchbook/MCD LabToLab/cartoTextes/encart");
String encartTxtOneShot ="";

int nb = 36;//------------------------------------------------------------------------------------------------ nombre de lab
int taillePolice = 10;//-------------------------------------------------------------------------------------- taille des polices (n'affectes pas les char)
float policeScale = 1;//-------------------------------------------------------------------------------------- agrandissement de la police
int numeroDePolice = 65;//------------------------------------------------------------------------------------ numero de la police
float facteurScale = policeScale;//--------------------------------------------------------------------------- pour le scale

int[][]index = {//-------------------------------------------------------------------------------------------- les différents anneaux de la rosace  
  new int[9], new int[8], new int[7], new int[6], new int[6]
};
int[]jump = {//----------------------------------------------------------------------------------------------- permet de répartir les listeDeNoms dans les listes de nom
  0,9,17,24,30
};
String [][] nameLabPart = {//--------------------------------------------------------------------------------- pour mettre les listeDeNoms dans les differents anneaux
  new String[index[0].length], new String[index[1].length], new String[index[2].length], new String[index[3].length], new String[index[4].length]
};

HashMap filRougeC = new HashMap();//-------------------------------------------------------------------------- stocke les couleurs en fonction des listeDeNoms
HashMap filRougeXY = new HashMap();//------------------------------------------------------------------------- stocke les oordonnées en fonction des listeDeNoms
HashMap filRougeA = new HashMap();//-------------------------------------------------------------------------- stocke les angles en fonction des listeDeNoms
HashMap filRougeNum = new HashMap();

Anneau anneau;//---------------------------------------------------------------------------------------------- l'anneau central
LAB lab;//---------------------------------------------------------------------------------------------------- la partie txt de l'anneau central
Encart encart;//---------------------------------------------------------------------------------------------- l'encart en bas a droite
Liste2Noms [] listeDeNoms = new Liste2Noms[5];//-------------------------------------------------------------- les 5 anneaux de noms dans la rosaces
MotClef [] motclef = new MotClef [nb];//---------------------------------------------------------------------- les mots clefs
Definition [] definition = new Definition [nb];//------------------------------------------------------------- definitions
Adresse [] adresse = new Adresse [nb];//---------------------------------------------------------------------- et adresse

//**************************************************************************************************************
//
//
//
//**************************************************************************************************************

void setup() {
  //size(1062,673);
  size(5314, 3366);
  randomSeed(35);
  smooth();

  anneau = new Anneau();
  lab = new LAB();

  for(int i=0; i<index.length; i++) {
   float[]distance = {//--------------------------------------------------------------------------------------- coefficient de multiplication pour le diametre de chaque cercle de listeDeNoms dans la rosace
      0.87, 0.77, 0.68, 0.57, 0.46
    };
    for(int j=0 ; j<index[i].length ; j++) {
      nameLabPart[i][j] = nameLab[j%nameLabPart[i].length + jump[i]];//---------------------------------------- chaque string contenu dans nameLabPart est issu de nameLab duquel on soustrait
    }
    String s = join(nameLabPart[i],"  ");//-------------------------------------------------------------------- les listes des listeDeNoms
    s = "  "+ s;//--------------------------------------------------------------------------------------------- début des phrases avec 2 espaces
    float start[] = new float[listeDeNoms.length];
    start[i] = random(TWO_PI);//------------------------------------------------------------------------------- début du cercle de listeDeNoms dans la rosace
    listeDeNoms[i] = new Liste2Noms(s, distance[i], start[i], i);
  }

  //-------------------------------------------------------
  //-------------------------------------------------------

  float posCoefMC[] = new float[nb];//------------------------------------------------------------------------- determine le coef de mul du rayon pour la position du mot clef
  float posCoefDef[] = new float[nb];//------------------------------------------------------------------------ determine le coef de mul du rayon pour la position de la definition
  float posCoefAd[] = new float[nb];//------------------------------------------------------------------------- determine le coef de mul du rayon pour la position de l'adresse
  int numero[] = new int[nb];//-------------------------------------------------------------------------------- récupère le numéro de la liste de nom auquelest rattaché le mot-clef
  float[] choixD = {//----------------------------------------------------------------------------------------- valeur de départ pour le tirage au sort des coef des motclefs, en fonction de l'anneau
    1.51, 1.41, 1.31, 1.21, 1.1
  };
  float[] choixF = {//----------------------------------------------------------------------------------------- valeur de fin pour le tirage au sort des coef des motclefs, en fonction de l'anneau
    1.59, 1.49, 1.39, 1.29, 1.19
  };
  float[] choixDdef = {
    3.1, 2.73, 2.40, 2.0, 1.62
  };
  float[] choixFdef = {
    3.12, 2.75, 2.42, 2.05, 1.65
  };
  float[] choixDadd = {
    3.70, 3.6, 3.54, 3.47, 3.4
  };
  float[] choixFadd = {
    3.71, 3.62, 3.56, 3.49, 3.41
  };

  for(int i=0 ; i<nb ; i++) {
    Iterator j = filRougeNum.entrySet().iterator();//---------------------------------------------------------- on recupere 
    while(j.hasNext()) {
      Map.Entry me = (Map.Entry) j.next();
      String s = (String) me.getKey();
      int c = (Integer) me.getValue();
      //if(motclef[i].ref.equalsIgnoreCase(s) == true) { 
      if(nameLab[i].equalsIgnoreCase(s) == true) { 
        numero[i]=c;
      }
    }
    posCoefMC[i] = random(choixD[numero[i]], choixF[numero[i]]);
    posCoefDef[i] = random(choixDdef[numero[i]], choixFdef[numero[i]]);
    posCoefAd[i]  = random(choixDadd[numero[i]], choixFadd[numero[i]]);
    motclef[i] = new MotClef(motCles[i], nameLab[i], posCoefMC[i]);
    definition[i] = new Definition(definitions[i], nameLab[i], posCoefDef[i]);
    adresse[i] = new Adresse(adresses[i], nameLab[i], posCoefAd[i] );
  }

  for(int k=0; k<encartTxt.length; k++) {
    encartTxtOneShot += encartTxt[k];
  }

  encart = new Encart(encartTxtOneShot);
}

//**************************************************************************************************************
//
//
//
//**************************************************************************************************************

void draw() {
  background(255);
  //  background(#CEFBFF);
  //  background(#FFFF6F);
  anneau.display();
  lab.display();
  for(int i=0; i<index.length; i++) {
    listeDeNoms[i].display();
  }

  for(int i=0 ; i<nb ; i++) {
    int col[] = new int[nb];
    float angle[] = new float[nb];

    PVector pos[] = new PVector[nb];
    pos[i] = new PVector();

    Iterator j = filRougeC.entrySet().iterator();//--------------------------------------- pour parcourir la hashmap des couleurs

    while(j.hasNext()) {
      Map.Entry me = (Map.Entry) j.next();
      String s = (String) me.getKey();
      float c = (Float) me.getValue();
      if(motclef[i].ref.equalsIgnoreCase(s) == true) { 
        col[i]=(int)c;
      }
    }
    Iterator k = filRougeA.entrySet().iterator();//--------------------------------------- pour parcourir celle des angles
    while(k.hasNext()) {
      Map.Entry me = (Map.Entry) k.next();
      String s = (String) me.getKey();
      float a = (Float) me.getValue();
      if(motclef[i].ref.equalsIgnoreCase(s) == true) { 
        angle[i]=a;
      }
    }
    Iterator xy = filRougeXY.entrySet().iterator();
    while(xy.hasNext()) {
      Map.Entry me = (Map.Entry) xy.next();
      String s = (String) me.getKey();
      PVector v = (PVector) me.getValue();
      if(motclef[i].ref.equalsIgnoreCase(s) == true) { 
       /* strokeWeight(2);
         stroke(col[i]);
         noFill();
         beginShape();
         //line((motclef[i].position.x + motclef[i].tailleMot/2), motclef[i].position.y, v.x, v.y);
         curveVertex(motclef[i].position.x , motclef[i].position.y);
         curveVertex(motclef[i].position.x , motclef[i].position.y);
         curveVertex(random(motclef[i].position.x , v.x), random(motclef[i].position.y, v.y));
         curveVertex(v.x, v.y);
         curveVertex(v.x, v.y);
         endShape();*/
        pos[i].add(v);
      }
    }

    motclef[i].display(angle[i], col[i], pos[i]);
    definition[i].display(angle[i], col[i], pos[i]);
    adresse[i].display(angle[i], col[i], pos[i]);
    encart.display();
    int recursion = 44;
    anneau.displayAgain(recursion);
  }

  if (savePDF) beginRecord(PDF, timestamp()+".pdf");
  if (savePDF) {
    savePDF = false;
    endRecord();
  }
}

//************************************************************************************
//
//                      ~~ si on presse une touche ...
//
//*************************************************************************************

void keyReleased() {
  // if (key == 's' || key == 'S') saveFrame("/home/algo/sketchbook/MCD LabToLab/CARTE_SEMANTIQUE_VERSION_G/police_65/"+timestamp()+"_##.jpg");
  if (key == 's' || key == 'S') saveFrame("/home/algo/sketchbook/MCD LabToLab/CARTE_SEMANTIQUE_VERSION_G/police_"+numeroDePolice+"/"+timestamp()+"_##.jpg");
  //if (key == 's' || key == 'S') saveFrame(timestamp()+"_##.jpg");
  if (key == 'p' || key == 'P') savePDF = true;
}

//************************************************************************************
//
//                      ~~ récupère la date et heure précise pour la sauvegarde
//
//*************************************************************************************


String timestamp() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", now);
}

