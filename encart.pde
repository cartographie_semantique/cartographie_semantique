class Encart {

  PFont f;
  String monEncart;
  float sizeLetter;
  float tailleMot;
  float hauteurMot;
  PVector position = new PVector();
  String[] monEncartPart = new String[1];//---------------------------------------------------------------------- notre tableau de string qui va s'agrandir au fur et à mesure du nombrede passage à la ligne
  float largeurEncart = 800;
  
//**************************************************************************************************************
//
//
//
//**************************************************************************************************************

  Encart(String _s) {
    monEncart = _s;
    sizeLetter = taillePolice;
    String[] fontList = PFont.list();
    f = createFont(fontList[numeroDePolice], taillePolice);
    textFont(f, taillePolice);
    textSize(sizeLetter);
    tailleMot = textWidth(monEncart);
    float ascent = textAscent();
    float descent = textDescent();
    hauteurMot = (ascent + descent);

    //------------------------------------------------------------------------------------------------------- algo pour passez à la ligne quand x espaces sot rencontrés dans le string
    int nbEspace = 0;//-------------------------------------------------------------------------------------- variable pour comptabiliser les espaces 
    int nbEspaceTolere = 7;//--------------------------------------------------------------------------------- nombre d'espaes tolérés
    int num = 0;//------------------------------------------------------------------------------------------- numéro du string en cour
    boolean aLaLigne = false;//------------------------------------------------------------------------------ vérifié si le nombre d'espace justifie un passage à la ligne

      String neo ="";//---------------------------------------------------------------------------------------- le string temporaire, qui récupère les string entre chaque saut de ligne

    for(int i=0; i<monEncart.length(); i++) {
      char c = monEncart.charAt(i); 
      if(c !='*') { 
        neo += str(c);//--------------------------------------------------------------------------------------- on ajoute les char au stringtempraire tant que la condtion n'est pas vérifiée
      }
      if(c == ' ') {//--------------------------------------------------------------------------------------- on continue de comptabiliser les espaces
        nbEspace++;
      }

      if(nbEspace == nbEspaceTolere || c == '*') {
        aLaLigne = true;
        nbEspace=0;
      }
      else {
        aLaLigne = false;
      }

      if(aLaLigne) {
        monEncartPart = expand(monEncartPart, num+1);//----------------------------------------------------------------- on agrandit le tableau de string de la valeur num qui  augmente elle meme à chaque passage a la ligne
        neo = trim(neo);
        monEncartPart[num] = neo; //------------------------------------------------------------------------------ on met le string temporaire dans le nouveau string du tableau
        neo="";//------------------------------------------------------------------------------------------- on réinitialise le string temporaire
        num++;
      }
    }
  }
  
//**************************************************************************************************************
//
//
//
//**************************************************************************************************************

  void display() {
    float policeScaleSpecialEcart = 1;

    for(int i=0; i<monEncartPart.length; i++) {
      float total =0, neoTotal=0;//, Wfactor =0;
      float x=0,y=0;
      float _tailleL[]  = new float[monEncartPart[i].length()];
      float _neoTailleL[]  = new float[monEncartPart[i].length()];
      char [] lettre = new char[monEncartPart[i].length()];

      for(int j=0; j<monEncartPart[i].length(); j++) {
        lettre [j] = monEncartPart[i].charAt(j);
        if(j>0) {
          _tailleL[j] = textWidth(lettre[j-1])*policeScaleSpecialEcart;
        }
        total += _tailleL[j];
      }

      for(int j=0; j<monEncartPart[i].length(); j++) {
        _neoTailleL[j] = (_tailleL[j] * largeurEncart) / total;//----------------------------------------------- important pour réajuster la taille de chque char au redimensionnement
      }


      for(int j=0; j<monEncartPart[i].length(); j++) {

        neoTotal += _neoTailleL[j];
        x = 6.7*(width/8) + neoTotal;
        y = 7.21*(height/8);
        pushMatrix();
        translate(x, y);//-------------------------------------------------------------------- on place les sdef au bon endroit
        scale(policeScaleSpecialEcart);
        if(j>=0 && j<=12 && i==0) {
          fill(80,75);
          text(lettre[j], 0, 0+(i*(hauteurMot*2)));
        }     
        if(j>=4 && j<=35 && i==2) {
          fill(100,75);
          text(lettre[j], 0, 0+(i*(hauteurMot*2)));
        }
        if(j>=38 && j<=58 && i==14) {
          fill(100,75);
          text(lettre[j], 0, 0+(i*(hauteurMot*2)));
        }
        fill(175,75);
        text(lettre[j], 0, 0+(i*(hauteurMot*2)));

        popMatrix();
      }
    }
  }
}

