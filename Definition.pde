class Definition {

  PFont typoDef;
  String def, ref;
  float sizeLetter, hauteurMot, distrib;
  PVector position = new PVector();
  ArrayList container = new ArrayList();//------------------------------------------------------------------ pour contenir es lettres avant de les remttres dans le tableau de strings
  String[] defpart = new String[1];//---------------------------------------------------------------------- notre tableau de string qui va s'agrandir au fur et à mesure du nombrede passage à la ligne
//  float lRect = 200;
//  float hRect = 150;

  //************************************************************************************************************************
  //
  //
  //
  //*************************************************************************************************************************

  Definition(String _def, String _ref, float _distrib) {
    def = _def;//-------------------------------------------------------------------------------------------- la définition du lab
    ref = _ref;//-------------------------------------------------------------------------------------------- on remet le nom du lab pour pouvoir gérer les hashmap
    distrib = _distrib;//------------------------------------------------------------------------------------ rayon de distribution sur lequeserot disposées les définitions
    sizeLetter = taillePolice;//--------------------------------------------------------------------------------------- tout est dis
    String[] fontList = PFont.list();
    typoDef = createFont(fontList[numeroDePolice], taillePolice);//---------------------------------------------------------------- on choisit la typo
    textFont(typoDef, taillePolice);
    textSize(sizeLetter);
    float ascent = textAscent();//--------------------------------------------------------------------------- pour calculer la hauteur des lignes
    float descent = textDescent();
    hauteurMot = (ascent + descent);

    //------------------------------------------------------------------------------------------------------- algo pour passez à la ligne quand x espaces sot rencontrés dans le string
    int nbEspace = 0;//-------------------------------------------------------------------------------------- variable pour comptabiliser les espaces 
    int nbEspaceTolere = 5;//--------------------------------------------------------------------------------- nombre d'espaes tolérés
    int num = 0;//------------------------------------------------------------------------------------------- numéro du string en cour
    boolean aLaLigne = false;//------------------------------------------------------------------------------ vérifié si le nombre d'espace justifie un passage à la ligne

      String neo ="";//---------------------------------------------------------------------------------------- le string temporaire, qui récupère les string entre chaque saut de ligne

    for(int i=0; i<def.length(); i++) {
      char c = def.charAt(i);
      if(c !='_') { 
        neo += str(c);//--------------------------------------------------------------------------------------- on ajoute les char au stringtempraire tant que la condtion n'est pas vérifiée
      }
      if(c == ' ') {//--------------------------------------------------------------------------------------- on continue de comptabiliser les espaces
        nbEspace++;
      }

      if(nbEspace == nbEspaceTolere || c == '_') {
        aLaLigne = true;
        nbEspace=0;
      }
      else {
        aLaLigne = false;
      }

      if(aLaLigne) {
        defpart = expand(defpart, num+1);//----------------------------------------------------------------- on agrandit le tableau de string de la valeur num qui  augmente elle meme à chaque passage a la ligne
        defpart[num] = neo; //------------------------------------------------------------------------------ on met le string temporaire dans le nouveau string du tableau
        neo="";//------------------------------------------------------------------------------------------- on réinitialise le string temporaire

        num++;
      }
    }
    /*
    for(int i=0; i<defpart.length; i++) {
     println(defpart[i]);
     }*/
     //println(defpart.length);
  }

  //************************************************************************************************************************
  //
  //
  //
  //*************************************************************************************************************************

  void display(float a, int col, PVector pos) {//------------------------------------------------------------ on recupere l'angle, la couleur, le coef pour le rayon et la position

    /*
    for(int i=0; i<defpart.length; i++) {
     char [] lettres = defpart[i].toCharArray();
     float largeurTotale = 0;
     for(int k =0 ;k<lettres.length; k++) {
     float ll = textWidth(lettres[k]);
     largeurTotale += ll;
     }
     }*/

    for(int i=0; i<defpart.length; i++) {
      float total =0;
      float _tailleL[]  = new float[defpart[i].length()];
      float _angleL [] = new float[defpart[i].length()];//------------------------------------------------------------------------------------ angle pour chaque lettre
      char [] lettre = new char[defpart[i].length()];
      float x = 0;
      float angleDecalage = 0;
      x = textWidth(defpart[i])/2;
      angleDecalage = x/((anneau.r/2)*distrib);//---------------------------------------------------------------------------------------- pourrecentrer le mot clefs avec le nom dulab

        for(int j=0; j<defpart[i].length(); j++) {
        lettre [j] = defpart[i].charAt(j);
      }

      //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
      for(int j=0;j<defpart[i].length()/2 ; j++) {//----------------------------------------------------------------------------------------- petit algo pour permuter lordre deschar du tableau (merci developpez.net)
        char temporaire = lettre [j];
        lettre [j] = lettre [defpart[i].length()-1 -j];
        lettre [defpart[i].length()-1 -j] = temporaire;
      } 
      //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
      for(int j=0; j<defpart[i].length(); j++) {
        if(lettre[j] == 'i' || lettre[j] == 'I') {//--------------------------------------------------------------------------------si la lettre est un 'i', on rajoute a sa taille pour éviter qu'elle disparaisse
          _tailleL[j] = textWidth(lettre[j])+2;//+textWidth(part[i])/2;
        }
        else {
          _tailleL[j] = textWidth(lettre[j])*facteurScale;
        }
        total += _tailleL[j];
        _angleL[j] = total / ((anneau.r/2)*distrib);
        _angleL[j] -= angleDecalage ;

        position.x = anneau.x + cos(a+_angleL[j]) *((anneau.r/2)*distrib);//------------------------------------------------ calcul la position x de chaque mot-clef
        position.y = anneau.y + sin(a+_angleL[j]) *((anneau.r/2)*distrib);

        //rectMode(CENTER);
        pushMatrix();
        translate(position.x, position.y);//-------------------------------------------------------------------- on placeles def au bon endrot
        rotate(a +_angleL[j]+ (3*PI)/2);
        scale(policeScale);
        fill(col);
        //        float transp = map(col,0,255,255,0);
        //        fill(160,24,0,transp);
        //textAlign(CENTER);
        //for(int i=0; i<part.length; i++) {
        //for(int i=0; i<defpart.length; i++) {
        text(lettre[j], 0, 0+(i*(hauteurMot*1.9)));
        // }
        /*noFill();
         stroke(col);  
         rectMode(CORNER);
         rect(0,0,lRect,hRect);*/
        popMatrix();
        //    strokeWeight(2);
        //    stroke(0,100);
        //      line(position.x, position.y, anneau.x, anneau.y);
      }
    }
  }
}

