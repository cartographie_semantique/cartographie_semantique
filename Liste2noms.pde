class Liste2Noms {

  float segmentName;//------------------------------------------------------------------------ angle entre chaque lettre
  String name;//------------------------------------------------------------------------------ nom du lab
  float distrib;//---------------------------------------------------------------------------- coefficient d'espacement entre chaque cercle de mots
  ArrayList MonNom = new ArrayList();
  float perimetre;
  int num;//---------------------------------------------------------------------------------- numero de la liste de noms

  //*******************************************************************************************
  //
  //
  //
  //*******************************************************************************************

  Liste2Noms(String _name, float _distrib, float start, int _num) {
    name = _name;//---------------------------------------------------------------------------- nom du lab (pour l'instant une suite de noms)
    num = _num;
    float arclength = 0;
    float total = 0;
    //----------------------------------------------------------------------------------------  on divise par le nombre total de lettre moins les espaces
    //----------------------------------------------------------------------------------------- pour obtenir les angles cartesiens
    for(int i=0; i<name.length() ; i++) {
      if(name.charAt(i) !='|') {
        total++;
      }
    }

    segmentName = 360f/ total;//--------------------------------------------------------------- l'angle entre chaque lettres en cartésien
    distrib = _distrib; 
    perimetre = 2 * PI * ((anneau.r/2)*distrib);
    float tailleFont = perimetre/total;//------------------------------------------------------ calcul pour la taille des lettres en fonction du perimetre / nbr total de lettres

    String[]liste = split(name, '|');//-------------------------------------------------------- le string principal est segmenté à chaque espace en un tableau de string
    for(int i=0; i<liste.length ; i++) {
      char [][] l = new char[liste.length][liste[i].length()];//------------------------------- contiendra les lettres de chaque nom
      float[][] a = new float[liste.length][liste[i].length()];//------------------------------ contiendra les angles de chaque lettre
      float[][] x = new float[liste.length][liste[i].length()];
      float[][] y = new float[liste.length][liste[i].length()];
      float[][] tF = new float[liste.length][liste[i].length()];
      color[] c = new color[liste.length];//---------------------------------------------------- contiendra les couleur des noms
      float[] tirage = new float[liste.length];
      String[]ref = new String[liste.length];//------------------------------------------------- contiedra les noms qui serviront de clé pour les hashmap TRES IMPORTANT
      //****************************************************************************************
      //
      //          ici choix des couleurs
      //
      //***************************************************************************************
      int[] choixD = {//------------------------------------------------------------------------ valeur de départ pour le tirage au sort des couleurs, en fonction de l'anneau
        0, 40, 85, 110, 130
      };
      int[] choixF = {//------------------------------------------------------------------------ valeur de fin pour le tirage au sort des couleurs, en fonction de l'anneau
        30, 75, 100, 120, 137
      };


      for(int j=0; j<liste[i].length() ; j++) {
        arclength += radians(segmentName);
        a[i][j] = start - arclength;
        x[i][j] = (width/ 2) + cos(a[i][j]) *  ((anneau.r/2)*distrib);
        y[i][j] = (height/2) + sin(a[i][j]) *  ((anneau.r/2)*distrib);

        float _x=0, angle_x=0;

        l[i] = liste[i].toCharArray();
        tirage[i] = random(choixD[num], choixF[num]);//-------------------------------------------- tirage au hasard de la couleur
        c[i] = color(tirage[i], tirage[i], tirage[i]);
        ref[i] = trim(liste[i]);
        tF[i][j] = tailleFont +(random(3));

        filRougeC.put(ref[i]+"|", tirage[i]);//------------------------------------------------ on met dans la hashmap filrougec, les couleurs de chaque nom
        filRougeA.put(ref[i]+"|", a[i][(a[i].length/2)+1]);//---------------------------------- on met dans la hashmap filrougea, les angles de la lettre au milieu de chaque nom
        filRougeXY.put(ref[i]+"|", new PVector(x[i][(x[i].length/2)+1], y[i][(y[i].length/2)+1]));
        filRougeNum.put(ref[i]+"|", num);//---------------------------------------------------- on met dans la hashmap filrougenum, le numero de la liste de nom correspondant
      }
      //---------------------------------------------------------------------------------------- pour calculer le milieu desnoms il faut partir de 2 et non de 0 !!!
      for(int j=2; j<liste[i].length(); j++) {        
        filRougeA.put(ref[i]+"|", a[i][(a[i].length/2)+1]);//---------------------------------- on met dans la hashmap filrougea, les angles de la lettre au milieu de chaque nom
      }
      MonNom.add(new Nom(l[i], a[i], x[i], y[i], c[i], tF[i]));
    }
  }


  //*******************************************************************************************
  //
  //
  //
  //*******************************************************************************************

  void display() {
    for(int i=0 ; i<MonNom.size(); i++) {//--------------------------------------------------- on scanne la arraylist et on récupère les noms a l'interieur, on leur applique la fonction display()
      Nom n = (Nom) MonNom.get(i);
      n.display();
    }
  }
}

