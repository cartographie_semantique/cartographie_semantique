class Adresse {

  PFont typoAdd;
  String addr, ref;
  float sizeLetter;
  float tailleMot;
  float hauteurMot;
  PVector position = new PVector();
  float distrib;
  
//**************************************************************************************************************
//
//
//
//**************************************************************************************************************

  Adresse(String _add, String _ref, float _distrib) {
    addr = _add;
    ref = _ref;
    distrib = _distrib;
    sizeLetter = taillePolice;
    String[] fontList = PFont.list();
    typoAdd = createFont(fontList[numeroDePolice], taillePolice);
    textFont(typoAdd, taillePolice);
    textSize(sizeLetter);
    tailleMot = textWidth(addr);
    float ascent = textAscent();
    float descent = textDescent();
    hauteurMot = (ascent + descent);
  }
  
//**************************************************************************************************************
//
//
//
//**************************************************************************************************************

  void display(float a, int col, PVector pos) {
    float total =0, xVertex1=0, yVertex1=0;
    float x = 0;
    float angleDecalage = 0;
    float _tailleL[]  = new float[addr.length()];
    float _angleL [] = new float[addr.length()];//------------------------------------------------------------------------------------ angle pour chaque lettre
    char [] lettre = new char[addr.length()];
    x = textWidth(addr)/2;
    float xBezier = textWidth(addr)/14;
    float angleBezier = xBezier/((anneau.r/2)*distrib);
    angleDecalage = x/((anneau.r/2)*distrib);//---------------------------------------------------------------------------------------- pour recentrer le mot clefs avec le nom dulab
    //    println(x+"   "+angleDecalage);
    for(int j=0; j<addr.length(); j++) {
      lettre [j] = addr.charAt(j);
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    for(int j=0;j<addr.length()/2 ; j++) {//----------------------------------------------------------------------------------------- petit algo pour permuter lordre deschar du tableau (merci developpez.net)
      char temporaire = lettre [j];
      lettre [j] = lettre [addr.length()-1 -j];
      lettre [addr.length()-1 -j] = temporaire;
    }  
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    for(int j=0; j<addr.length(); j++) {
      if(lettre[j] == 'i' || lettre[j] == 'I') {//--------------------------------------------------------------------------------si la lettre est un 'i', on rajoute a sa taille pour éviter qu'elle disparaisse
        _tailleL[j] = textWidth(lettre[j])+4;//+textWidth(part[i])/2;
      }
      else {
        _tailleL[j] = textWidth(lettre[j])*facteurScale;
      }
      total += _tailleL[j];
      _angleL[j] = total / ((anneau.r/2)*distrib);
      _angleL[j] -= angleDecalage ;

      position.x = anneau.x + cos(a + _angleL[j]) *((anneau.r/2)*distrib);//------------------------------------------------ calcul la position x de chaque mot-clef
      position.y = anneau.y + sin(a + _angleL[j]) *((anneau.r/2)*distrib);

      xVertex1 = anneau.x + cos(a + angleBezier) *((anneau.r/2)*distrib);
      yVertex1 = anneau.y + sin(a + angleBezier) *((anneau.r/2)*distrib);

      pushMatrix();
      translate(position.x, position.y);
      scale(policeScale);
      rotate(a+_angleL[j]+ (3*PI)/2);
      fill(col);
      float transp = map(col,0,255,255,0);
      text(lettre[j], 0, 0);//+(i*hauteurMot)); 
      popMatrix();

      beginShape();
      noFill();
      strokeWeight(0.2);
      stroke(col);//,175);
      vertex(position.x, position.y);
      // bezierVertex(xVertex1,yVertex1,xVertex1,yVertex1, anneau.x, anneau.y);
      bezierVertex(xVertex1,yVertex1,xVertex1,yVertex1, pos.x, pos.y);
      endShape();
    }
  }
}

